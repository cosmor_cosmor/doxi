# -*- coding: utf-8 -*-
#
# dx-console -  management interface to a mongodb-containig naxsi-events
#
#

version = "0.0.3.84 alpha-5 - 2013-07-21"


import sys, hashlib, time, memcache, glob, operator, json, pymongo, os
from flask.ext.script import Manager, Command
from bson.objectid import ObjectId

sys.path.append("lib")

from doxi_console import *
from dx_console import app
manager = Manager(app)

#~ 

charset='utf-8'

def rolog(log_line, status="OK"):
        f = open(rlog, "a")
        f.write("%s - %5s - %s\n" % (time.strftime("%Y-%m-%d %H:%M:%S"), status.upper(), log_line))
        f.close()
        return()
    


@manager.command
def robot():

    # check if mtime - tnow < 2/3 valid -> sys.init
    
    # generate backups
    
    # run usercron
    
    # daily stats: mail with status / new messages etc
    
    st = t_now()
    print "> running robot"
    rolog("> starting robot_run")
    et = t_now()
    latest = set_latest_sig()
    #~ print latest
    try:
        lt = latest["timestamp"]
    except:
        lt = t_now()
        
    if not os.path.isfile(rlog):
        print "> init robot"
        
        f = open(rstat, "w")
        f.write("""latest sig   : %s
written       : %s
""" % (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(float(lt))), time.strftime("%Y-%m-%d %H:%M:%S") ))
        f.close()
        tnow = int(time.time())
        print "> updating robot_stats_file"
        os.utime(rstat,(tnow, int(lt)))
        rolog("robot_init")
        
    if not "timestamp" in latest:
        f = open(rlog, "a")
        f.write("%s - [ERR] -  cannot extract timestamp from latest sig\n" % time.strftime("%Y-%m-%d %H:%M:%S"))
        f.close()
    
        msg_admin("ERROR while trying to run robot/no timestamp in latest")
        return()
    
    sa = {}
    ft = os.stat(rstat)[8]
    sa["tstamp"] = { "$gte":  ft }
    new_sigs = coll_events.find(sa).count()
    if new_sigs == 0:
        print "  > no new sigs found"
        return()
    new_sigs = coll_events.find(sa)
    new = 0
    for sig  in new_sigs:
        sid = sig["_id"]
        if coll_events.find({"_id" : ObjectId(sid)}).count() > 0:
            ces = check_uniq_stats(sid)
        
            if len(ces) > 1:
                print "> new uniq_events : %s" % ces["msg"]
                rolog("new_uniq: %s" % ces["msg"])
                new = 1
            else:
                print "> NO new uniq_events : %s" % sid

        else:
            print "  > no event found for id: %s" % sid

    if new == 1:
        f = open(rstat, "w")
        f.write("""latest sig   : %s
    written       : %s
    """ % (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(float(lt))), time.strftime("%Y-%m-%d %H:%M:%S") ))
        f.close()
        tnow = int(time.time())
        print "> updating robot_stats_file"
        os.utime(rstat,(tnow, int(lt)))

    agents_run()
        
    #~ 
    #~ f = open(user_stat_file, "w")
    #~ f.write("""latest sig   : %s
#~ written     : %s""" % (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(float(timestamp))), time.strftime("%Y-%m-%d %H:%M:%S") ))
    #~ f.close()
    #~ tnow = int(time.time())
    #~ print "> updating user_session_file"
    #~ os.utime(user_stat_file,(tnow, int(timestamp)))
#~ 
    #~ 
    #~ 
    
    
if __name__ == "__main__":
    d_data = app.config["DATA_DIR"]
    rlog = "%s/logs/robot.log" % d_data
    rstat = "%s/run/robot.stat" % d_data 

    manager.run()
