

# Writing NAXSI-Signatures

please read http://code.google.com/p/naxsi/wiki/HowDoesItWork first
to understand the nature of naxsi-sigs.


# Rules
- MainRule  -> define a detection-pattern and scores
- BasicRule -> define whitelists for MainRules
- CheckRule -> define actions, when a score is met

# MainRule

The MainRule - Identifier is used to mark detection-rules, in opposite to BasicRules who are usually 
used to whitelist certain MainRules. 



    MainRule "msg:this is a message" "str:searchstring"  "mz:URL|BODY|ARGS" "s:$XSS:8" id:12345678990;
     |          |                      |                   |                 |          +-> UNIQE ID     
     |          |                      |                   |                 |
     |          |                      |                   |                 +-> SCORE
     |          |                      |                   |
     |          |                      |                   +-> MZ
     |          |                      |
     |          |                      +-> SearchString/RegEx
     |          |
     |          +-> MESSAGE
     |
     +-> RuleDesignator

    Designators and its values MUST be wrapped in quotionmarks "dsg:[pattern]" 

## MainRulePattern

### RuleDesignator
- must be MainRule

### SearchString
- define a searchstring with "str:searchstr" or regex-pattern with "rx:reg.+ex" 
Naxsi does case insensitive matching on strings if your string is lowercase !
string match is *way* faster than regex

in case you want to use heavy pcre-statements (see https://groups.google.com/group/naxsi-discuss/browse_thread/thread/a6efabbffb6c6b7c)

reply by bui:
    Please don't do this ! 
    I written naxsi exactly in order not to have to do this. 
    I don't want to have complex/evolved rules/patterns, but rather focus 
    on primitives used by attacks.

### MZ: Matching Zones

With matching-zones define the  area of a request your search-pattern will apply. Valid designatores are

- URL -> full URI (server-path of a request)
- ARGS -> Request-Arguments (all the stuffe behind ? in a GET-Request 
- BODY -> Request-Data from a POST-Request (http_client_body)
- $HEADERS_VAR:[value] -> any HTTP-HEADERS-var that is available, eg
  - $HEADERS_VAR:User-Agent
  - $HEADERS_VAR:Cookie
  - $HEADERS_VAR:Content-Type
  - $HEADERS_VAR:Connection
  - $HEADERS_VAR:Accept-Encoding


### creating a sig for POST:

- include BODY in MZ for searching the body of the POST - request
- include mz:$URL/hallo/test|BODY for a sig on a post to a certain URL, 
"str:Submit=Run" "mz:$URL:/script|$BODY_VAR:Submit"

# WhiteListing

**ATTENCIONE!!!** Whitelisting should always be done via BasicRule, 
thus MUST be included in location {} - context!

you CANNOT Whitelist via MainRule. 


## generic whitelist for a certain


    BasicRule wl:1234; # generic whitelist for rule 1234 for all requests

## generic whitelist on a certain URL

    BasicRule wl:1100 "mz:$URL:/some/url|URL";


# Fail2Ban

~~~ 

# jail.conf
[nginx-naxsi]

enabled = true
port = http,https
filter = nginx-naxsi
logpath = /var/log/nginx/error.log
maxretry = 3
banaction = iptables-multiport-log
action = %(action_mwl)s


~~~

# WebApp - Specific WhiteLists

## DokuWiki

BasicRule wl:1315 "mz:$HEADERS_VAR:cookie";



