
<div align="left" markdown="1">

## User-Administration


- [Link: /admin/users](/admin/users)



#### User-Info:

- data/users/$user -> file with basic user_info 
    - name
    - email
    - hashed_pw
    - admin y/n
    - generated oid (you should never change this)
    - 
    
- data/users/$user.data/
    - $user.info
        - user_profile_information about mails be sended'n'stuff
        - might be edited by user 
    - api.key -> keeps an api-key; empty file if no key given
    
    
