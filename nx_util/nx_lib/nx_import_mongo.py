
import sys, time, datetime

def nx_mongo(buf, mongo):    
    #print self.dict_buf
    print len(buf)

    sys.path.append("../lib")
    sys.path.append("../web")
    latest_file = "./nx_util.latest"

    from doxi_console import url_convert
    from mongoengine import connect 
    mongodb_host = mongo[0]
    mongodb_port = mongo[1]
    mongodb_db   = mongo[2]
    mongodb_active = mongo[3]
    connect(db=mongodb_db, host=mongodb_host, port=mongodb_port)
    from naxsi_db_schema import Event as nx_event
    from socket import getfqdn, gethostbyname
    sensor = getfqdn()


    latest_date = 0
    count = 0
    known = 0
    new = 0 
    # getting the latest for this sensor
    latest = nx_event.objects(sensor_id = sensor).only('tstamp').limit(1).order_by('-tstamp')
    
    for l in latest:
        latest_date = int(l.tstamp)
        print latest_date
        break

    for entry in buf:
        #mongo-import
    
        count += 1
    
        url         = url_convert(entry['uri'])
        zone        = entry['zone']
        var_name    = entry['var_name']
        rule_id     = entry['id']
        content     = entry['content']
        peer_ip     = entry['ip']
        host        = entry['server']
        dstamp      = str(entry['date'])
        rmks        = ""
        # converting date_stamp to unixtime
        
    
    
        try:
            ts_1 = dstamp.split()
            ts_11 = ts_1[0].split("-")
            ts_21 = ts_1[1].split(":")
            itime = datetime.datetime(int(ts_11[0]), int(ts_11[1]), int(ts_11[2]), int(ts_21[0]), int(ts_21[1]), 0).timetuple()
            
            tstamp =    int(time.mktime(itime))    
    
        except:
            tstamp = int(time.time())
            dstamp = None
        #print "%s vs %s" % (tstamp, latest_date)
        
        if tstamp <= latest_date:
            #print "entry is known - %16s - %8s" % (peer_ip, rule_id)
            known += 1
            continue 

        print "new_entry - %16s - %8s" % (peer_ip, rule_id)

        try:
            gethostbyname(host)
        except:
            rmks = """unknown/invalid host_header: %s""" % host
            print rmks
            host = "s|%s" % sensor



    
        #mo_res = nx_event(host = host, url=url, zone = zone, var_name = var_name, rule_id = rule_id, content = content, peer_ip = peer_ip, dstamp = dstamp, tstamp = tstamp ).save()
        
        try:
#            print " nx_event(host = host, url=url, zone = zone, var_name = var_name, rule_id = rule_id, content = content, peer_ip = peer_ip, dstamp = dstamp, tstamp = tstamp, sensor = sensor, remarks = rmks ).save()"
            mo_res = nx_event(host = host, url=url, zone = zone, var_name = var_name, rule_id = rule_id, content = content, peer_ip = peer_ip, dstamp = dstamp, tstamp = tstamp, sensor_id = sensor, remarks = rmks ).save()
        except:
            print "cannot insert :: %s | %s | %s | %s | %s | %s" % (rule_id, host, peer_ip, zone, url, var_name)
        
        new += 1
 
    print "total: %s / known: %s / new: %s " % (count, known, new)
