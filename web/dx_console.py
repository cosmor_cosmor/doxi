# -*- coding: utf-8 -*-
#
# dx-console -  an interface to a mongodb-containig naxsi-events
#
#

version = "0.4.4.32 alpha-4 - 2013-08-10"

import sys, hashlib, time, memcache, glob, operator, json, pymongo

sys.path.append("lib")


from flask import Flask, render_template, Response, request, make_response, redirect, flash
from werkzeug.contrib.fixers import ProxyFix
from saferproxyfix import SaferProxyFix
from functools import wraps
from flask.ext.mongoengine import MongoEngine
from bson.objectid import ObjectId
from datetime import datetime

from naxsi_db_schema import Event as nx_event

from doxi_lib import *
from doxi_console import *



app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)
app.wsgi_app = SaferProxyFix(app.wsgi_app)

#~ 

charset='utf-8'




app.config['MONGODB_HOST'] = "localhost"
app.config['MONGODB_PORT'] = 17017
app.config['MONGODB_DB']  = "naxsi_events"
app.config['NXUTIL_DIR'] = "../nx_util"

app.config["MONGODB_SETTINGS"] = {
                'DB'    : app.config['MONGODB_DB'], 
                'HOST'  : app.config['MONGODB_HOST'], 
                'PORT'  : app.config['MONGODB_PORT'],
                }
# obsolete
#app.config["USER"] = "admin"
#app.config["PASS"] = "breznak"
app.config['MEMCACHE_CACHETIME'] = 1800 # 30 minutes should be ok
app.config['SECRET'] = "%s" % hashlib.sha512("2ab0d85fb9c4354b5970f7de2ab0c518d5788fe71f86c77501e739c70571a259aa0261c1c9032617dd12d52c00966f39f0729452c58120877962153c91c28456").hexdigest()[0:100].encode(charset)
app.secret_key = app.config['SECRET']
app.config["DISPLAY_LIMIT"] = 200
app.config["DEFAULT_HOST"] = "all"
app.config['DEFAULT_TIME_RANGE'] = "7d"
app.config['DATA_DIR'] = "./data"
app.config['ROBOT'] = 1
app.config['ROBOT_INTERVAL'] = 120
app.config['PAGE_REFRESH_INTERVAL'] = 300






dtr = app.config['DEFAULT_TIME_RANGE']

ctime = app.config['MEMCACHE_CACHETIME']

db = MongoEngine(app)



title = "DX-CONSOLE"
page = 1
section = None
DEBUG=True

sess_default = {
    "time_span": "%s" % dtr,
    
    }




def init():
    # db-indexing
    # http://docs.mongodb.org/manual/tutorial/create-indexes-to-support-queries/
    # http://docs.mongodb.org/manual/tutorial/sort-results-with-indexes/
    # http://docs.mongodb.org/manual/tutorial/create-queries-that-ensure-selectivity/
    print """
-------------------------------------------------
> starting engines %s 
    
    """ % (time.strftime("%F- %H:%M", time.localtime(time.time())))

    st = int(time.time())
    mc = memcache.Client(['127.0.0.1:11211'], debug=0)
    mc.flush_all()

    generate_results()
    app.jinja_env.globals['version'] = version
    mc.set("%s.active" % app.config['SECRET'], "yezz", time=cache_time)
    et = int(time.time())
    wt = et - st
    
    print "> start_time: %s s" % wt
    
def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        global sess

        mactive = mc.get("%s.active" % app.config['SECRET'])
        print "mactive: %s" % mactive 
        if not mactive:
            flash("[+] regenerating dashboard_caches")
            msg_admin("[+] sys_cache_init")
            init()



            
    
        
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()    

        try:
            sess = request.cookies.get('dxid').encode("utf-8")
        except:
            flash('Your sessions seems to have expired, creating new')
            sess = "invalid"



        cs = mc.get("%s" % (sess))
        if cs == None:
            print "got no session?"
            sess = hashlib.sha512("%s.%s.%s" % (app.config["SECRET_KEY"], time.time(), auth.username)).hexdigest().encode(charset)[0:128]            
            resp = make_response(redirect(request.url))
            resp.set_cookie('dxid', sess)
            print "setting dxid: %s" % sess
            uu = au[auth.username]
            usess = {}
            usess["name"] = uu["name"]
            usess["range"] = 86400
            usess["range_name"] = "24h"
            usess["hosts"] = uu["hosts"]
            usess["sensors"] = uu["sensors"]
            usess["admin"] = uu["admin"]
            usess["email"] = uu["email"]
            usess["search"] = ["", ""]
            usess["uid"]     = uu["uid"]
            usess["latest_sig"] = os.stat("%s/users/%s.data/latest.sig" % (d_data, usess["name"]))[8]
            mc.set("%s" % (sess), sess_default, time=86400)
            mc.set("%s.session" % (sess), usess)
            return resp
        
        
        
        return f(*args, **kwargs)
    return decorated


    

def check_range(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        sess = request.cookies.get('dxid').encode(charset)

        print " checking range"
        global si, page 

        
        t_range = 86400
        ts = 86400   # timerange - step
        tn = "1d" # timerange - name
        
        g_r = ""

        u_sess = mc.get("%s.session" % sess)
        if u_sess == None:
            print "[-] no session while checking range"
            print sess 
            return authenticate()
        
        si = {}
        si = u_sess
        si["id"] = sess 
        #~ si["name"] =  u_sess["name"]
        #~ si["email"] =  u_sess["email"]
        #~ si["range"] = u_sess["range"]
        #~ si["range_name"] = u_sess["range_name"]
        #~ si["sensors"] = u_sess["sensors"]
        #~ si["hosts"] = u_sess["hosts"]
        #~ si["admin"] = u_sess["admin"]
        #~ si["search"] = u_sess["search"]
        #~ si["latest_sig"] = u_sess["latest_sig"]
        #~ si["uid"] = u_sess["uid"] 

        g_page = request.args.get('page', '')
        if not g_page:
            page = 0

        g_range = request.args.get('range', '')
        if not g_range:
            print "s.range -> defaults : %s" % si["range"]
    
        if g_range:
            print "checking grange from request"
            print "in: %s :" % g_range
            t_range, tn = range_check(g_range)

            si["range"] = t_range
            si["range_name"] = tn
            update_user_session("range", t_range)
            update_user_session("range_name", tn)
            print "out: %s : %s " % (t_range, tn)
        

        # check for host
        g_host = request.args.get('host', '')
        if not g_host:
            print "checking set_host from mc"
            print si["hosts"]

    
        if g_host:
            print "checking set_host from request"
            
            

        # check for new sigs 
        set_latest_sig()
        lus = (si["latest_sig"])
        app.jinja_env.globals['latest_user_sig'] = lus
        lss = get_latest_sig()
        if lus < lss["timestamp"]:
            print "oups, new events found"
            ns = nx_event.objects(tstamp__gt=lus).count()
            flash("""%s new events!!!""" % ns)
            app.jinja_env.globals['new_events'] = ns
            if d_robot == 0:
                # check for new uniq_stuff if not using robot
                nc = coll_events.find({"tstamp": { "$gte" : lus}})
                for c in nc:
                    cid = c["_id"] 
                    nms = check_uniq_stats(cid)
                    if len(nms) > 0:
                        flash("%s " % nms["msg"])
                        
        # checking for new messages 

        print "> range_checking messages"
        umsg = coll_msgs.find({"tag": "%s_msgs_list" % si["name"]})
        uk = []
        for u in umsg:
            uk = u["data"] 
            break 

        nms = {}
        nms["admin"] = 0
        nms["new_uniq"] = 0
        sa = {"tag": {"$in": ["admin_messages", "new_event"]}}
        ma = coll_msgs.find(sa).sort("tstamp", DESC)
        new = 0
        for m in ma:
            mmsg = {}
            mid   = str(m["_id"])
            mtag  = m["tag"]
            mmsg["time"] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(float(m["data"]["tstamp"])))
            if mid in uk:
                continue
                #print "   > msg is acked %s "% mid 
            else:
                #print "   > NEW msg is un_ack %s "% mid 
                new += 1
                if mtag == "admin_messages":
                    nms["admin"] += 1

                elif mtag == "new_event":
                    nms["new_uniq"] += 1
        print "new msgs: %s" % new
        app.jinja_env.globals['new_admin_msg'] = nms["admin"]
        app.jinja_env.globals['new_uniq_msg'] = nms["new_uniq"]
        app.jinja_env.globals['new_msg'] = new
        
                


        is_admin = si["admin"]
        try:
            if int(is_admin) == 1:
                app.jinja_env.globals['admin'] = 1
        except:
            pass
            
        u_search = si["search"][0]
        u_range  = si["search"][1]
        app.jinja_env.globals['search_term'] = u_search
        app.jinja_env.globals['search_range'] = u_range

        print "> setting session_cache_info"
        mc.set("%s.session" % sess, si)
        mc.set("%s" % sess, "%s" % sess, 86400)
        
        
        trange = si["range"]
        tname  = si["range_name"]
        app.jinja_env.globals['range'] = si["range"]
        app.jinja_env.globals['range_name'] = si["range_name"]
        app.jinja_env.globals['reload_header'] = """<meta http-equiv="refresh" content="%s" > """ % app.config['PAGE_REFRESH_INTERVAL'] 

        return f(*args, **kwargs)
    return decorated
    
def check_admin(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if si["admin"] != 1:
            return(redirect("/u-is-no-admin/"))
        return f(*args, **kwargs)
    return decorated


@app.route('/',  methods=['GET'])
@requires_auth
@check_range
def d_index():
    return(redirect("/dashboard-index/"))


@app.route('/login/',  methods=['POST', 'GET'])
def d_login():
    undo_reload_header()
    error = None
    if request.method == 'POST':
        try:
            login = request.form['login']
            pw    = request.form['password']
        except:
            return redirect(url_for('d_login'))
        
        if request.form['login'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('show_entries'))
    return render_template('login.html', error=error)


@app.route('/admin/',  methods=['GET'])
@app.route('/admin/<path:area>/',  methods=['GET', 'POST'])
@app.route('/admin/<path:area>/<path:action>/',  methods=['GET', 'POST'])
@requires_auth
@check_range
@check_admin
def d_admin(area=0, action=0):
    sensor_list = get_sensorinfos()
    undo_reload_header()
    
    if area == "users":
        generate_userdicts()
        global au
        au = json.loads(mc.get("%s.users" % mc_key))

        print "userz"
        print "> action: %s" % action

        if action == 0:
           return render_template("users.html", users = au)
            
        elif action in au:
            
            user = action
            uv = au[user]
            return render_template("user.html", 
                    user_info = uv, 
                    sensor_list = sensor_list,
                    )

        elif action == "new":
            uv = {}
            uv["name"] = "NEW"
            uv["email"] = ""
            
            return render_template("user.html", 
                    user_info = uv, 
                    sensor_list = sensor_list,
                    admin = None
            
                    )

        elif action == "del":
            try:
                udel   = request.form['user_del']
            except:
                flash("cannot delete:")
                return(redirect("/admin/users/"))

            user_file = "%s/users/%s" % (d_data, udel)
            if not os.path.isfile(user_file):
                flash("cannot find user_file for %s" % udel)
                return(redirect("/admin/users/"))
            try:
                os.unlink(user_file)
            except:
                flash("cannot delete user_file for %s" % udel)
                return(redirect("/admin/users/"))

            flash("[+] successfully deleted user_file for %s" % udel)
            return(redirect("/admin/users/"))
            


    
        elif action in ("save", "save_new"):
            print "> action save"
            if request.method == 'GET':
                return(redirect("/admin/users/"))


            try:
                uname   = request.form['uname'].strip().lower()
                umail   = request.form['umail'].lower()
                uadmin  = request.form['uadmin']
                upass   = request.form['upass']
                hselect = request.form['hselect']
                

            except:
                print "> error while retrieving data from form"
                print request.form
                return(redirect("/admin/users/new/"))


            # TODO
            # check if username contains non_alphanum_chars

            #~ if action == "save_new":
                #~ if uname in 
            

            if hselect == "sensors":
                usensors = [request.form['xsensor']]
                
                uhosts = []
            else:
                usensors = []
                uhosts = request.form['xhost']

            try:
                if int(uadmin) == 1:
                    uadmin = 1
                else:
                    uadmin = 0
            except:
                uadmin = 0

            if len(uname) < 3:
                flash("username must have at least 3 chars")
                return(redirect("/admin/users/new/"))
                

            new_pw = 0 
            if uname in au:
                print "> know user: %s" % uname 
                flash("saving known user: %s" % uname)
                if upass == "":
                    # getting password from all_user_dict
                    upass = au[uname]["pass"]
                    uid   = au[uname]["uid"]
                else:
                    new_pw = 1    
            else:
                flash("creating new user: %s" % uname)
                print "> new user: %s " % uname 
                new_pw = 1

            if new_pw != 0:
                if len(upass) < 8:
                    up = hashlib.sha512("%s.%s.%s.%s" % (app.secret_key, time.time(), uname, umail)).hexdigest()[10:23].encode(charset)
                    pwc = PwCheck(uname, upass)
                    upass = pwc.pw_hash
                    flash("no password provided or too short (8chars minimum), setting: %s" % up)
            uid = hashlib.sha512(uname + upass + mc_key).hexdigest()
            user_file = "%s/users/%s" % (d_data, uname)
            f = open(user_file, "w")
            f.write("""
password    : %s
email       : %s
sensors     : %s
hosts       : %s 
admin       : %s
active      : 1
uid         : %s

            """ % (upass, umail, ",".join(usensors), ",".join(uhosts), uadmin, uid))
            flash("successfully writen user_file for: %s" % uname )
            os.mkdir("%s/users/%s.data/" % (d_data, uname))
            user_stat_file =  "%s/users/%s.data/latest.sig" % (d_data, uname)
            f=open(user_stat_file, "w")
            f.write("")
            f.close()
            os.utime(user_stat_file(t_now(), si["latest_sig"]))
            return(redirect("/admin/users/"))

    elif area == "system":

        return render_template("system.html")

    elif area == "ip-reputation":
        ip_list = "%s/ip-reputation.list" % d_data
        if not os.path.isfile(ip_list):
            f = open(ip_list, "w")
            f.write("""# ip-reputation-list - generated
            
            
            
            """)
            f.close()
        
        
        ips = {}
        fip = open(ip_list).readlines()
        for ip in fip:
            ip = ip.strip()
            if len(ip) < 4:
                continue
            elif ip[0] == "#":
                continue
            else:
                ix = ip.split(":")[0].strip()
                try:
                    im = ip.split(":")[1].strip()
                except:
                    im = "unknown"
                ips[ix] = im
        
        
        return render_template("ip-reputation.html", 
            ip_list = ips )
        
    elif area == "clean":
        
        if action == "dashboard":
        
            init()
            flash("[+] rebuild dashboard_cache and temp_data")
            return(redirect("/dashboard/"))

        elif action == "uniq_stats":
            delete_uniq_data()
            flash("[+] rebuild uniq_stats and temp_data")
            return(redirect("/dashboard/"))

        else:
           return render_template("index.html", ix = render_content("clean_caches"))


    elif area == "delete_events":

        print "delete_events"
            
        
        try:
            osrch = request.form['srch']
            srch   = request.form['srch'].lower()
            t_range = request.form['trange'].lower()
            rd = request.form['delete'].lower() # really delete?
            fp_name = request.form['fp_name'].lower()
            
        except:
            print "> error while trying to get infos for deleting "
            return(redirect("/search-dont-know-how-to-delete/"))
        
        
        if len(srch) < 5:
            return(redirect("/search-not-enough-string/"))



        app.jinja_env.globals['search_term'] = osrch
        app.jinja_env.globals['search_range'] = t_range

        res, c_data, u_ip, u_sid, u_host = event_search(srch, time_range = t_range, skip = 0, cdata = 0 )

    

        did = []
        for ex in res:
            did.append(ex[10])
        
        print did
        
        qry = {"_id": { "$in": did}}
    
        print "> deleting ...."
        coll_events.remove(qry)
        print "   > done ...."
        

        flash("deleted %s events from event_db:: search: %s | %s" % (len(res), srch, t_range))


        if rd == "yes":
            return(redirect("/dashboard-after-deletion/"))

        # ok, now storing result 
        if len(fp_name) < 3:
            store_title = "%s | %s" % (srch, t_range)
        else:
            store_title = "%s" % (fp_name)
        store_time = t_now()
        store_tag = "false_positives"
        store_data = res
        udata(tag=store_tag, tstamp = store_time, data = store_data, title = store_title, search="%s | %s" % (srch, t_range)).save()
        flash("saved %s search: %s | %s" % (len(res), srch, t_range))
        return(redirect("/dashboard-after-marking-fp/"))
        

        

        

    else:
       return render_template("index.html", ix = render_content("admin"))
        

        
    return(redirect("/dashboard/"))

@app.route('/audit/',  methods=['GET'])
@requires_auth
@check_range
def d_audit(page=0):
    print "audit"
#    from naxsi_db_schema import Event as nx_event


    s_range = si["range"]
    sel_dat = int(int(time.time() - s_range))


    peer_ip = request.remote_addr
    # this for testing only
#    a_r    = nx_event.objects(tstamp__gte = sel_dat).order_by("-tstamp")
#    a_u    = nx_event.objects().distinct("rule_id")
    a_r    = nx_event.objects(peer_ip = peer_ip).limit(1000).order_by("-tstamp")
    a_u    = nx_event.objects(peer_ip = peer_ip).distinct("rule_id")

    total_events = len(a_r)
    my_events = []
    my_uniq = []
    
    
    for e in a_r:
        sid = e.rule_id
        date = e.dstamp
        try:
            s_msg = sigs_dict["%s" % sid][0]
        except:
            s_msg = "%s" % sid
        my_events.append((sid, s_msg, date))

    for i in a_u:
        sid = i
        u_c =  nx_event.objects(rule_id = i, peer_ip = peer_ip).count()
        try:
            s_msg = sigs_dict["%s" % sid][0]
        except:
            s_msg = "%s" % sid
        my_uniq.append((sid, s_msg, u_c))


    phead = """Audit your own IP<br> <a href="/do/search/ip@%s">%s</a>""" % (peer_ip, peer_ip)
    return render_template('audit.html', 
            page_header = phead, 
            total_events = total_events, 
            data=my_events, 
            uniq=sorted(my_uniq, key=lambda my_uniq: my_uniq[2], reverse=True), 
            remote_ip = peer_ip,)
    

@app.route('/audex/',  methods=['GET'])
def d_audex(page=0):
    print "audit_external"
#    from naxsi_db_schema import Event as nx_event


    s_range = (86400*7) # 7 days back
    sel_dat = int(int(time.time() - s_range))


    peer_ip = request.remote_addr
    # this for testing only
#    a_r    = nx_event.objects(tstamp__gte = sel_dat).order_by("-tstamp")
#    a_u    = nx_event.objects().distinct("rule_id")
    a_r    = nx_event.objects(peer_ip = peer_ip).limit(1000).order_by("-tstamp")
    a_u    = nx_event.objects(peer_ip = peer_ip).distinct("rule_id")

    total_events = len(a_r)
    my_events = []
    my_uniq = []
    
    
    for e in a_r:
        sid = e.rule_id
        date = e.dstamp
        try:
            s_msg = sigs_dict["%s" % sid][0]
        except:
            s_msg = "%s" % sid
        my_events.append((sid, s_msg, date))

    for i in a_u:
        sid = i
        u_c =  nx_event.objects(rule_id = i, peer_ip = peer_ip).count()
        try:
            s_msg = sigs_dict["%s" % sid][0]
        except:
            s_msg = "%s" % sid
        my_uniq.append((sid, s_msg, u_c))


    phead = """Audit your own IP<br> %s""" % (peer_ip)
    return render_template('audex.html', 
            page_header = phead, 
            total_events = total_events, 
            data=my_events, 
            uniq=sorted(my_uniq, key=lambda my_uniq: my_uniq[2], reverse=True), 
            remote_ip = peer_ip,)




@app.route('/doc/',  methods=['GET'])
@app.route('/doc/<path:page>/',  methods=['GET'])
@requires_auth
@check_range
def d_doc(page=0):
    undo_reload_header()
    if page == "sig_list":
        sig_sorted = sorted(sigs_dict.iteritems(), key=operator.itemgetter(0))
        return render_template('sig_list.html', sig_list = sig_sorted)

    elif page == "todo":
        tcontent = render_content("todo") 
        return render_template('index.html', ix = tcontent)

    elif page == "search":
        tcontent = render_content("doc/search_hints") 
        return render_template('index.html', ix = tcontent)


    elif page == "features":
        tcontent = render_content("doc/features") 
        return render_template('index.html', ix = tcontent)


    elif page == "setup":
        tcontent = render_content("doc/setup") 
        return render_template('index.html', ix = tcontent)

    elif page == "agents":
        tcontent = render_content("doc/agent_help") 
        return render_template('index.html', ix = tcontent)


    else:
        doc_content = render_content("doc/documentation") + render_content("search_hints")
        return render_template('index.html', ix = doc_content)



@app.route('/do/',  methods=['POST', 'GET'])
@app.route('/do/<path:action>/',  methods=['POST', 'GET'])
@app.route('/do/<path:action>/<path:subaction>/',  methods=['GET'])
@requires_auth
@check_range
def do(action=0, subaction = 0, xid = 0):
    undo_reload_header()
    
    uid = si["uid"]
    
    if request.method == 'GET':
        if subaction == 0:
            return(redirect("/dashboard/"))
    if action == 0:
        return(redirect("/dashboard/"))

    elif action == "search":
        try:
            osrch = request.form['srch']
            srch   = request.form['srch'].lower()
            t_range = request.form['trange'].lower()
            page = subaction
        except:
            print "[..] subaction: %s" % subaction
            if subaction == 0:
                return(redirect("/search/"))

            page = 0
            if subaction.find("__") > -1:
                srch = " | ".join(subaction.split("__")) 
                osrch = srch
                t_range = "30d"
            elif subaction.find("@"):
                srch = " = ".join(subaction.split("@")) 
                osrch = srch
                t_range = "30d"
            if subaction == "eventlist":
                print "GOGOGOGO"
                srch = "ip = *"
                osrch = srch
                t_range = "24h"
            elif subaction == "latest_24h":
                srch = "ip = *"
                osrch = srch
                t_range = "24h"
            elif subaction == "latest_7d":
                srch = "ip = *"
                osrch = srch
                t_range = "7d"
            elif subaction == "latest_14d":
                srch = "ip = *"
                osrch = srch
                t_range = "14d"
            elif subaction == "latest_30d":
                srch = "ip = *"
                osrch = srch
                t_range = "30d"
                
        
        
        if len(srch) < 5:
            return(redirect("/search/"))

        

        app.jinja_env.globals['search_term'] = osrch
        app.jinja_env.globals['search_range'] = t_range
        
        so = si
        so["search"] = [osrch, t_range]
        mc.set("%s.session" % sess, so)

        page_next, page_prev, skip = check_page(page)
        
        res, c_data, u_ip, u_sid, u_host = event_search(srch, time_range = t_range, skip = skip, cdata = 1 )
        
        #print res 
        if len(res) > 0:
            if res[0] == "ssearch":
                # special_search 
                phead="Search"
                page_info = "search"
                return render_template("ssearch.html", 
                        page_header = phead, 
                        total_events = len(c_data),
                        thead=res[1],
                        ssearch = res[2],  
                        data=sorted(c_data,  key=lambda c_data: c_data[1], reverse=True),
                        page_info = page_info,
                        )


        app.jinja_env.globals['page_prev'] = page_prev
        app.jinja_env.globals['page_next'] = page_next

        if len(t_range) > 1:
            tr, tn = range_check(t_range)
            update_user_session("range", tr)
            update_user_session("range_name", tn)
            app.jinja_env.globals['range_name'] = tn
            app.jinja_env.globals['range']      = tr
             

        # generating chart_data
        chart_data = {}
        c_type = "area"
        chart_data["Search_Count"] =  chart_dashboard_template % ("Search_Count", c_type, "Search_Count","Events",  json.dumps(c_data[0]), json.dumps(c_data[1]) )

    
        #~ for i in uniq_ip:
            #~ i_c = nx_event.objects(tstamp__gte = sel_dat, peer_ip = i).count()        
            #~ u_i.append((i, i_c))
    #~ 
        #~ for i in uniq_sig:
            #~ i_c = nx_event.objects(tstamp__gte = sel_dat, rule_id = i).count()        
            #~ u_e.append((i, i_c))
    
        phead="Suche"
        total_events = len(res)
        u_i = u_e = []
        #page_next = pageprev = None
        page_info = "suche"
        

    
        return render_template("search.html", 
                page_header = phead, 
                total_events = total_events,
                data=res, 
                uniq_ips = sorted(u_ip, key=lambda u_ip: u_ip[1], reverse=True), #sorted(student_tuples, key=lambda student: student[2]) 
                uniq_events = sorted(u_sid, key=lambda u_sid: u_sid[1], reverse=True), 
                uniq_hosts = sorted(u_host, key=lambda u_host: u_host[1], reverse=True), 
                page_next = page_next,
                page_prev = page_prev,
                page_info = page_info,
                charts = chart_data
                )


    elif action == "display":
        if subaction == "mystats":
            pass 
            

    elif action == "sig_search":
        srch = request.form['srch']
        res = sig_search(srch)
        return render_template("sig_suche.html")

    elif action in ["message_ack", "agent_ack"]:
        print "> msg_ack"
        if subaction == 0:
            return(redirect("/display/messages/"))
        print "> msg_ack"
        user = si["name"]
        utag = "%s_msgs_list" % user
        umsg = coll_msgs.find({"tag" : "%s" % utag})
        print "umsg: %s" % umsg
        nu = []
        for m in umsg:
            nu = m["data"]
            break 
        
        print nu
        if subaction == "all_events":
            print "> msg_ack :: all_events"
            tag = "new_event"
        elif subaction == "all_agents":
            print "> msg_ack :: all_agents"
            tag = "agent_message"

        elif subaction == "all_admin":
            print "> msg_ack :: system_msgs"
            tag = "admin_messages"
        else:
            tag = 0
            nu.append(subaction)

        #~ else:
            #~ return(redirect("/msgg-ack-nono/"))
            

        mmsg = coll_msgs.find({"tag" : tag})
        for m in mmsg:
            mid = str(m["_id"])
            if mid in nu:
                print "> msg_ack found :: %s" % mid
                continue
            else:
                print "> msg_ack NOT found, appending :: %s" % mid
                nu.append(mid)
            
        print "> updating user_msg_list"
        ts = coll_msgs.remove({"tag": utag})
        ts = coll_msgs.save({"tag": utag, "data" :nu})
    
        flash("updated user_msgs_list :: %s" % action)
        if action == "message_ack":
            return(redirect("/display/messages/"))
        elif action == "agent_ack":
            return(redirect("/display/agents/msgs/"))

        #update_user_msgs(si["name"], subaction)
            
        
        
        return(redirect("/display/messages/"))

    elif action == "new_agent":
        print "> new_agent"
        print request.form
        try:
            kname   = request.form['aname'].lower().strip()
            khost   = request.form['host'].lower().strip()
            ksid    = request.form['sid'].lower().strip()
            kip     = request.form['ip'].lower().strip()
            kint    = request.form['interval'].strip()
            kcnt    = request.form['count'].strip()
            
        except:
            flash("error in: %s" % request.form)
            return(redirect("/display/agents/"))

        
        if len(kname) < 3:
            flash("you need to set a agent_name > 2 chars")
            return(redirect("/display/agents/"))

        try:
            int(kcnt)
        except:
            flash("count muste be an integer > your value:  %s" % kcnt)
            return(redirect("/display/agents/"))


        # generating result 
        
        an = {}
        
        aid = random_string(16)
        
        # TODO
        khost = "all"
        
        if khost == "all":
            pass 
        else:
            an["host"] = { "$in": [khost]}
        
        # creating search_pattern for sids    
        as_list = []
        try:
            int(ksid)
            as_list = [int(ksid)]
        except:
            
            if len(ksid) > 1:
                if ksid.find(",") > -1:
                    ks = ksid.split(",")
                    for k in ks:
                        k.strip()
                        as_list.append(int(k))
                else:
                    ks = ksid.split(" ")
                    for k in ks:
                        k.strip()
                        as_list.append(int(k))
            
        if len(as_list) > 0:
            an["rule_id"] = { "$in" : as_list}
        

        # checking search_pattern for ips
        aip_list = []
        if len(kip) > 1:
            if ksid.find(",") > -1:
                ki = kip.split(",")
                for k in ki:
                    k.strip()
                    aip_list.append(k)
            else:
                ki = kip.split(" ")
                for k in ki:
                    k.strip()
                    aip_list.append(k)
            
        if len(aip_list) > 0:
            an["peer_ip"] = { "$in" : aip_list}
        
        
        # checking search_pattern for run_interval
        a_int = "24h"
        if kint in ["5m", "1h" "24h"]:
            a_int = kint
        
        if a_int == "5m":
            timestep = 300
        elif a_int == "1h":
            timestep = 3600
        elif a_int == "24h":
            timestep = 86400
        else:
            a_int = "24h"
            timestep = 86400
        
        # generating data_field
        a_d = {}
        a_d["name"]     = kname
        a_d["timestep"] = timestep
        a_d["search"]   = an
        a_d["count"]    = int(kcnt)
        
        # default_status -> 3 -> MUST choose auto_run or manual 
        a_s  = agents(tag="agent", data=a_d, user_id = si["uid"], agent_id = aid, next_run = 0, status = 0 ).save()
        flash("agent [ %s :: %s ] has been saved, is set to manual execution" % (kname, aid))
        return(redirect("/display/agents/"))

    elif action == "agent_mod":
        if subaction == 0:
            return(redirect("/display/agents/"))
        sc = subaction.split("_")
        if len(sc) != 2:
            return(redirect("/display/agents/"))
        action = sc[0]
        aid = sc[1]
        if action == "activate":
            ts = agents.objects(agent_id=aid).update(set__status = 1)
            flash("[+] agent has been activated: %s" % aid)
            return(redirect("/display/agents/"))
        elif action == "deactivate":
            ts = agents.objects(agent_id=aid).update(set__status = 0)
            flash("[+] agent has been set_manual: %s" % aid)
            return(redirect("/display/agents/"))
        elif action == "delete":
            ts = agents.objects(agent_id=aid).delete()
            flash("[+] agent has been deleted: %s" % aid)
            return(redirect("/display/agents/"))
        
        return(redirect("/display/agents/"))

    elif action == "agent_run":
        print "> agent_run : %s " % subaction
        if subaction == 0:
            return(redirect("/display/agents/"))
        aid = subaction
        a_r = agents.objects(user_id=si["uid"], agent_id = aid)
        srch = {}
        tstep = 86400
        for a in a_r:
            srch = a["data"]["search"]
            name = a["data"]["name"]
            tstep = int(a["data"]["timestep"])
            cnt = int(a["data"]["count"])
            aid   = a.agent_id
            break 
        s_a = {}
        
        if len(srch) > 0:
            s_a = srch
        
        print s_a
        
        tdiff = t_now() - tstep
        print tdiff
        s_a["tstamp"] = { "$gte": tdiff }
        print s_a
        a_e = nx_event.objects(tstamp__gte=tdiff).count()
        l_tag = "agent_log"
        l_id = aid
        l_msg = "agent_default_msg"
        l_data = {}
        l_status = 0
        if a_e >= cnt:
            l_msg = "[-] AGENT_HIT : %s  :: Count[ %s ] matched; total: %s" % (name, cnt, a_e)
            l_status = 1

        else:
            l_msg = "[+] AGENT_OK  : %s  :: Count[ %s ] OK; total: %s" % (name, cnt, a_e)

        ma = {}
        ma["msg"] = l_msg
        ma["status"] = l_status
        ma["name"] = name
        ma["aid"] = aid 
        if l_status > 0:
            msg_agent(ma)
        agent_log(ma)
        flash(l_msg)


        return(redirect("/display/agents/"))

    elif action == "export":
        print "do:export"
        try:
            area   = request.form['area'].lower()
            xid = request.form['id'].lower()
        except:
            return(redirect("/export-invalid-values"))
        
        if area == "fp":
            tag = "false_positives"
        elif area == "saved_search":
            tag = "saved_search_%s" % uid


        def generate():

            sres = coll_udata.find({"tag": tag, "_id": ObjectId(xid)})
            
            res_data = []
            for x in sres:
                res_data = x["data"] 
                res_title = x["title"]
                res_srch = x["search"]
                res_time = time.strftime("%Y-%m-%d %H:%M", time.localtime(float(x["tstamp"])))
                break 
                
#            print res_data
            for x in res_data:
                print x
            print "resss_data"
            yield """STATS Saved Search: \n Name: %s \n Search: %s \n Date: %s\n""" % (res_title, res_srch, res_time) 
            yield """,SID,PEER_IP,URL,HOST,MZ,ARG,Content,Sensor_ID,Date \n"""
            for row in res_data:
                yield """,%s,%s,"%s",%s,%s,%s,%s,%s,%s\n""" % (row[0], row[2], row[3], row[5], row[6], row[7], row[8], row[9], row[4])
        
        
        return Response(generate(), mimetype='text/csv')

    elif action == "save_search":

        print "do:save_events"
            
        
        try:
            osrch = request.form['srch']
            srch   = request.form['srch'].lower()
            t_range = request.form['trange'].lower()
            srch_name = request.form['name'].lower()
            
        except:
            print "> error while trying to get infos for deleting "
            return(redirect("/search-dont-know-how-to-save/"))

        if len(srch) < 5:
            return(redirect("/search-not-enough-string/"))



        app.jinja_env.globals['search_term'] = osrch
        app.jinja_env.globals['search_range'] = t_range

        res, c_data, u_ip, u_sid, u_host = event_search(srch, time_range = t_range, skip = 0, cdata = 0 )

    


        # ok, now storing result 
        if len(srch_name) < 3:
            store_title = "%s | %s" % (srch, t_range)
        else:
            store_title = "%s" % (srch_name)
        store_time = t_now()
        store_tag = "saved_search_%s" % si["uid"]
        store_data = res
        udata(tag=store_tag, tstamp = store_time, data = store_data, title = store_title, search="%s | %s" % (srch, t_range)).save()
        flash("saved %s search: %s | %s" % (len(res), srch, t_range))
        return(redirect("/dashboard-after-storing-searches/"))


    elif action == "whitelist":

        print "do:whitelist"

        try:
            wsid = request.form['wsid']
            wip   = request.form['wip'].lower()
            whost = request.form['whost'].lower()
            wrange = request.form['wrange'].lower()

        except:
            return(redirect("/whitelist-invalid-values"))
        
        wsrch = "ip = *"
        
        try:
            int(wsid)
            wsrch += " & id = %s " % wsid
        except:
            pass
        
        if wip == "":
            pass
        else:
            wsrch += " & ip = %s" % wip
        
        if whost == "":
            pass
        else:
            wsrch += " & host = %s" % whost 
        
        if wrange == "":
            wrange = "7d"

        
        all_e = event_search(wsrch, wrange,  skip = 0, cdata = 0 )[0]
        
        wl = {}
        c = 0
        for x in all_e:
            mzc = "" # mz-definition
            wlc = "" # identifier
            c += 1
            #wl[c] = x
            x_id = x[0]
            x_nm = x[1]
            x_ip = x[2]
            x_ul = x[3]
            x_ho = x[5]
            x_mz = x[6]
            x_vn = x[7] 

            wlc = "%s_%s_%s" % (x_id, x_mz, x_vn)
            
            if x_mz == "URL":
                mzc = "$URL:%s" % x_ul
                wlc += "_%s" % x_ul
            elif x_mz in("HEADERS", "ARGS", "BODY"):
                if x_vn != "":
                    mzc = "$%s_VAR:%s" % (x_mz, x_vn)
                else:
                    mzc = "%s" % x_mz
                if x_ul != "":
                    mzc2 = "%s|$URL:%s" % (mzc, x_ul)
                    wl["%s_url_%s" % (wlc, x_ul)] = [x_id, mzc2, x_nm]

            wl[wlc] = [x_id, mzc, x_nm]
            
        phead = """<h3><a href="/sid/%s/" class="btn">Back to %s</a>""" % (wsid, wsid)
        return render_template("whitelist.html", 
            whitelists = wl,
            page_head = phead)


    elif action == "ipcheck":
        print "do:ipreputation-check"
        rep_res = rep_check("all")
        if rep_res == 99:
            print "> unknown error"
            return(redirect("/repcheck-unknown-error/"))
        
        
        return(render_template("ip-result.html", 
            ip_found = rep_res))

    elif action == "del_search":
        print "do:del_searches_or_fp"
        try:
            area   = request.form['area'].lower()
            xid = request.form['id'].lower()
        except:
            return(redirect("/export-invalid-values"))
        
        if area == "fp":
            tag = "false_positives"
            area = "fps"
        elif area == "saved_search":
            tag = "saved_search_%s" % uid



        sres = coll_udata.remove({"tag": tag, "_id": ObjectId(xid)})
            
        
        return(redirect("/display/%s/" % area))


        
    else:
        return(redirect("/dashboard/"))


@app.route('/search/',  methods=['GET'])
@requires_auth
@check_range
def d_search(page=0):
    undo_reload_header()

    app.jinja_env.globals['search_term'] = ""
    app.jinja_env.globals['search_range'] = si["range"]

    search_content = render_content("search") + render_content("doc/search_hints")
    return render_template('index.html', ix = search_content)


@app.route('/charts/',  methods=['GET'])
@requires_auth
@check_range
def d_charts(page=0):



    search_content = render_content("search") + render_content("search_hints")
    return render_template('charts.html', ix = search_content)

    
@app.route('/display/<path:area>/',  methods=['GET'])
@app.route('/display/<path:area>/<path:ido>/',  methods=['GET'])
@requires_auth
@check_range
def d_display(area=0, ido = 0, adisplay=0):
    uid = si["uid"]
    undo_reload_header()
    if area == "session":
        st = """
-- user-session ------------------------
        """ 
        for i in si:
            st += "\n%-30s : %s" % (i, si[i])
        latest_sig = get_latest_sig()
        st += "\n\n--- latest_system_sig----\n"
        for l in latest_sig:
            st += "%-30s : %s\n" % (l, latest_sig[l])
        
        sess_txt = render_content("session") % (st)
        return render_template('index.html', ix = sess_txt)
    elif area == "messages":
        print "> messages"
        umsg = coll_msgs.find({"tag": "%s_msgs_list" % si["name"]})
        uk = []
        for u in umsg:
            uk = u["data"] 
            break 

        known = {}
        unknown = {}
        sa = {}
        sa["tag"] = "new_event"
        ma = coll_msgs.find(sa).sort("tstamp", DESC)
        for m in ma:
            mmsg = {}
            mid   = str(m["_id"])
            mmsg  = m["data"]
            mmsg["time"] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(float(m["data"]["tstamp"])))
            if mid in uk:
                print "msg is acked %s "% mid 
                known[mid] = mmsg 
            else:
                print "msg is un_ack %s "% mid 
                unknown[mid] = mmsg 
                



        sa = {}
        a_ack = {}
        a_noack = {}
        sa["tag"] = "admin_messages"
        adm = coll_msgs.find(sa).sort("tstamp", DESC)
        for a in adm:
            amsg = {}
            amsg = a["data"]
            aid   = str(a["_id"])[0:32]
            amsg["time"] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(float(a["data"]["tstamp"])))
            if aid in uk:
                #print "a.msg is acked %s "% aid 
                a_ack[aid] = amsg 
            else:
                #print "a.msg is un_ack %s "% aid 
                a_noack[aid] = amsg 
                


        
        return render_template('messages.html', 
            known_msgs = known,
            unknown_msgs = unknown,
            admin_new = a_noack,
            admin_old = a_ack,

            agent_new = a_noack,
            agent_old = a_ack,
            
        
        )

    elif area == "agents":
        
        if ido == 0:
            # default-display
        
            active_agents = {}
            s_a = agents.objects(user_id=si["uid"], status = 1, tag="agent").order_by("-next_run", "-timestamp")
            for a in s_a:
                a_id = a.agent_id
                a_n  = a.next_run
                a_data = a.data 
                a_data["next_run"] = a_n
                active_agents[a_id] = a_data
    
            print "active agents"
            print active_agents
    
    
            manual_agents = {}
            m_a = agents.objects(user_id=si["uid"], status = 0, tag="agent").order_by("-next_run", "-timestamp")
            for a in m_a:
                a_id = a.agent_id
                a_n  = a.next_run
                a_data = a.data 
                a_data["next_run"] = a_n
                manual_agents[a_id] = a_data
            
            print "manual agents"
            print manual_agents
            
    
            new_agent = render_content("new_agent")
            ag_doc = render_content("agent_help")
            return render_template('agents.html', 
    
                aa = active_agents,
                ia = manual_agents ,
                ne = new_agent,
                ad = ag_doc, 
                
                
            
            )

        elif ido == "log":

            # agent log display 
            my_agents = get_user_agents(uid)

            a_log = {}
            s_a = alog.objects(agent_id__in = my_agents, tag="agent_log").order_by("-timestamp")
            for a in s_a:
                l = {}
                l["status"]  = a.status 
                l["msg"]   = a.msg
                l_xid   = a.id
                l["data"]  = a.data 
                l["name"] = a.data["name"]
                
                a_log[l_xid] = l
    
            return render_template('agent.html', 
    
                alog = a_log,
                
                
            
            )


        elif ido == "msgs":

            # agent log display 
            my_agents = get_user_agents(uid)
            umsg = coll_msgs.find({"tag": "%s_msgs_list" % si["name"]})
            uk = []
            for u in umsg:
                uk = u["data"] 
                break 
            known = {}
            unknown = {}
            sa = {}
            sa["tag"] = "agent_message"
            sa["aid"] = {"$in": my_agents}
            
            ma = coll_msgs.find(sa).sort("tstamp", DESC)
            for m in ma:
                mmsg = {}
                mid   = str(m["_id"])
                mmsg  = m["data"]
                mmsg["time"] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(float(m["tstamp"])))
                if mid in uk:
                    print "msg is acked %s "% mid 
                    known[mid] = mmsg 
                else:
                    print "msg is un_ack %s "% mid 
                    unknown[mid] = mmsg 
                    
    
    
    
    
            
            return render_template('amessages.html', 
                known_msgs = known,
                unknown_msgs = unknown,

                
            
            )


    


        else:
            # agent log display 
            aid = ido

            a_log = {}
            s_a = alog.objects(agent_id = aid, tag="agent_log").order_by("-timestamp")
            for a in s_a:
                l = {}
                l["status"]  = a.status 
                l["msg"]   = a.msg
                l_xid   = a.id
                l["data"]  = a.data 
                l["name"] = a.data["name"]
                
                a_log[l_xid] = l
    
            print "logs for %s" % aid
            print a_log
    
    
            
            return render_template('agent.html', 
    
                alog = a_log,
                
                
            
            )


    elif area == "whitelist":
        return render_template('whitelist_gen.html', 
            hostlist = get_hostlist()
            
        
        )



    elif area == "fps":

        fp_list = {}
        fp_res  = coll_udata.find({"tag": "false_positives"})
        for f in fp_res:
            fpl = {}
            fpl["id"] = f["tstamp"]
            fpl["date"] = time.strftime("%Y-%m-%d %H:%M", time.localtime(float(f["tstamp"])))
            fpl["title"] = f["title"]
            fpl["search"] = f["search"]
            fpl["srch_raw"] = f["search"].split("|")[0]
            fpl["srch_range"] = f["search"].split("|")[1]
            fpl["count"] = len(f["data"])
            fpl["xid"]  = f["_id"]
            fp_list[fpl["id"]] = fpl
            

        return render_template('false_positives.html', 
            fp_list = fp_list,
            page_title = "False-Positives-List",
            page_area  = "fp", 
            
        
        )


    elif area == "saved_search":

        fp_list = {}
        fp_res  = coll_udata.find({"tag": "saved_search_%s" % si["uid"]})
        for f in fp_res:
            fpl = {}
            fpl["id"] = f["tstamp"]
            fpl["date"] = time.strftime("%Y-%m-%d %H:%M", time.localtime(float(f["tstamp"])))
            fpl["title"] = f["title"]
            fpl["search"] = f["search"]
            fpl["srch_raw"] = f["search"].split("|")[0]
            fpl["srch_range"] = f["search"].split("|")[1]
            fpl["count"] = len(f["data"])
            fpl["xid"]  = f["_id"]
            fp_list[fpl["id"]] = fpl
            

        return render_template('false_positives.html', 
            page_title = "Saved Searches",
            fp_list = fp_list,
            page_area  = "saved_search", 
            
        
        )

            
        
    return(redirect("/dashboard/"))


@app.route('/dashboard/',  methods=['GET'])
@requires_auth
@check_range
def d_dashboard():
    app.jinja_env.globals['total_events'] = "dashboard"

    xa = {}
    charts = {}
    for times in ( i30, i24, i7):        
        timen = times[1]
        timet = times[0]
        # sid-stats
        charts[timen] = {}
        for stat in stats_value:
            
            c_for = stat
            gd    = tmp.objects.filter(tag = "dash_%s_%s" % (c_for, timen))
            xx = []
            tc = 0
            for x in gd:
                s_all = x.data
                for a_s in s_all:
                    s_msg = ""
                    sig = a_s[0]
                    count = a_s[1]
                    if stat == "rule_id":
                        try:
                            s_msg = sigs_dict["%s" % sig][0]
                        except:
                            s_msg = "%s" % sig
                    xx.append((sig, count, s_msg))
                    tc += count 
            xa["%s.%s" % (timen, c_for)] = xx
            xa["%s.%s.count" % (timen, c_for)] = tc
            
            
            gd    = tmp.objects.filter(tag = "dash_chart_%s_%s" % (timen, c_for))
            c_dat = ""
            for rc in gd:
                c_dat = rc.data 
                break
            charts[timen][c_for] = c_dat 
            
        
            
            
        #~ gd    = tmp.objects.filter(tag = "dash_rule_id_%s" % timen)
        #~ xx = []
        #~ tc = 0
        #~ for x in gd:
            #~ s_all = x.data
            #~ for a_s in s_all:
                #~ sig = a_s[0]
                #~ count = a_s[1]
                #~ try:
                    #~ s_msg = sigs_dict["%s" % sig][0]
                #~ except:
                    #~ s_msg = "%s" % sig
                #~ xx.append((sig, count, s_msg))
                #~ tc += count 
        #~ xa["%s.sigs" % timen] = xx
        #~ xa["%s.sigs.count" % timen] = tc
#~ 
        #~ # ip-stats
        #~ gd    = tmp.objects.filter(tag = "dash_peer_ip_%s" % timen)
        #~ xx = []
        #~ tc = 0
        #~ for x in gd:
            #~ s_all = x.data
            #~ for a_s in s_all:
                #~ ip = a_s[0]
                #~ count = a_s[1]
                #~ xx.append((ip, count))
                #~ tc += count 
        #~ xa["%s.ips" % timen] = xx
        #~ xa["%s.ips.count" % timen] = tc
#~ 
        #~ # host-stats
        #~ gd    = tmp.objects.filter(tag = "dash_host_%s" % timen)
        #~ xx = []
        #~ tc = 0
        #~ for x in gd:
            #~ s_all = x.data
            #~ for a_s in s_all:
                #~ ip = a_s[0]
                #~ count = a_s[1]
#~ 
                #~ # its stupido ....
                #~ if len(ip.split(".")) < 2:
                    #~ ip = "unknow [ %s ]" % ip
                #~ 
                #~ xx.append((ip, count))
                #~ tc += count 
        #~ xa["%s.host" % timen] = xx
        #~ xa["%s.host.count" % timen] = tc
#~ 
        #~ # sensor-stats
        #~ gd    = tmp.objects.filter(tag = "dash_sensor_id_%s" % timen)
        #~ xx = []
        #~ tc = 0
        #~ for x in gd:
            #~ s_all = x.data
            #~ for a_s in s_all:
                #~ ip = a_s[0]
                #~ count = a_s[1]
                #~ xx.append((ip, count))
                #~ tc += count 
        #~ xa["%s.sensor" % timen] = xx
        #~ xa["%s.sensor.count" % timen] = tc
#~ 

        
        #~ print x.data[0], x.data[1]
        #~ s_id = x.data[0][0]
        #~ s_ct = x.data[0][1]
        #~ xa[s_id] = s_ct
    
    # generate charts_info
    # chart[chart_id] = chart_data (js_funktion)

    #~ return render_template('dash_new.html', 
        #~ 
        #~ data = dash_data, 
        #~ 
        #~ 
        #~ 
        #~ )

    return render_template('dashboard.html', 
        data24 = xa["24hrs.rule_id"], data7d = xa["7days.rule_id"], data30d = xa["30days.rule_id"], 
        l24 = xa["24hrs.rule_id.count"], l7 = xa["7days.rule_id.count"], l30 =xa["30days.rule_id.count"],
        ip24 = xa["24hrs.peer_ip"], ip7 = xa["7days.peer_ip"], ip30 = xa["30days.peer_ip"],
        li24 = xa["24hrs.peer_ip.count"], li7 = xa["7days.peer_ip.count"], li30 =xa["30days.peer_ip.count"],
        h24 = xa["24hrs.host"], h7 = xa["7days.host"], h30 = xa["30days.host"],
        lh24 = xa["24hrs.host.count"], lh7 = xa["7days.host.count"], lh30 =xa["30days.host.count"],
        s24 = xa["24hrs.sensor_id"], s7 = xa["7days.sensor_id"], s30 = xa["30days.sensor_id"],
        ls24 = xa["24hrs.sensor_id.count"], ls7 = xa["7days.sensor_id.count"], ls30 =xa["30days.sensor_id.count"],
        chart_d_24 = charts["24hrs"], 
        chart_d_7  = charts["7days"], 
        chart_d_30  = charts["30days"], 
        
        
        )
    

@app.route('/sid/',  methods=['GET'])
@app.route('/sid/<path:sig>/',  methods=['GET'])
@app.route('/sid/<path:sig>/<path:page>/',  methods=['GET'])
@requires_auth
@check_range
def d_sig_display(sig=0, page=0):
    print "sig_display"

    try:
        sig = int(sig)
    except:
        return(redirect("/dashboard/"))    
        

    tnow = int(time.time())

    if sig == 0:
        return(redirect("/dashboard/"))    

    try:
        int(page)
    except:
        return(redirect("/dashboard/"))    

    page_next, page_prev, skip = check_page(page)

    try:
        s_msg = sigs_dict["%s" % sig][0]
    except:
        s_msg = "%s" % sig

    
    
    s_range = si["range"]
    print "srange: %s" % si["range_name"]
    sel_dat = int(tnow - s_range)

    #~ if sig == 0:
        #~ sig_list    = nx_event.objects(tstamp__gte = sel_dat).skip(skip).order_by('-tstamp')
        #~ uniq_ip = nx_event.objects(tstamp__gte = sel_dat).distinct("peer_ip")
        #~ uniq_sig = nx_event.objects(tstamp__gte = sel_dat).distinct("rule_id")
        #~ phead = "SID: Summary "  
        #~ s_template = "sig_display.html"
        #~ d_all = "yes"
        #~ sid_info = [(None, None, None)]
    #~ else:
        #~ sig_list    = nx_event.objects(rule_id = sig, tstamp__gte = sel_dat).skip(skip).order_by('-tstamp')
        #~ uniq_ip = nx_event.objects(rule_id = sig, tstamp__gte = sel_dat).distinct("peer_ip")
        #~ uniq_sig = [sig]
        #~ phead = "SID: %s :: %s" % (sig, s_msg) 
        #~ s_template = "sig_display.html"
        #~ d_all = None

    sig_list    = nx_event.objects(rule_id = sig, tstamp__gte = sel_dat).skip(skip).order_by('-tstamp')
    uniq_ip = nx_event.objects(rule_id = sig, tstamp__gte = sel_dat).distinct("peer_ip")
    uniq_sig = [sig]
    phead = "SID: %s :: %s" % (sig, s_msg) 
    s_template = "sig_display.html"
    d_all = None

    
    sid_info = [sig, ["","","","",""]]
    s_l = []
    u_i = []
    u_e = []

    for s in sig_list:
        sid = s.rule_id
        date = s.dstamp
        url = s.url
        rip = s.peer_ip
        mz = s.zone
        var_name = s.var_name 
        host = s.host
        content = s.content
        idx = s.id
        try:
            s_msg = sigs_dict["%s" % sid][0]
            sid_info = [sid, sigs_dict["%s" % sid]]
        except:
            s_msg = "%s" % sid

        s_l.append((sid, s_msg, rip, url, date, mz, var_name, host, content, idx))

    for i in uniq_ip:
        if sig == 0:
            i_c = nx_event.objects(tstamp__gte = sel_dat, peer_ip = i).count()        
        else:
            i_c = nx_event.objects(rule_id = sig, tstamp__gte = sel_dat, peer_ip = i).count()
        u_i.append((i, i_c))

    for i in uniq_sig:
        i_c = nx_event.objects(tstamp__gte = sel_dat, rule_id = i).count()        
        u_e.append((i, i_c))


    # generating chart_data
    chart_data = {}
    
    if s_range < 86401:
        c_step = 3600
    else:
        c_step = 86400

    # uniq_ips
    i_val = []
    i_desc = []
    for step in range((tnow - s_range), tnow, c_step):
        sel = {}
        #print "%s - %s - %s" % (step, tnow, c_step)
        start = step  
        stop  = step + c_step 
        #print "%s:%s - %s - %s" % (times, c_for, start, stop)
        
        # getting descriptor
        dk = "%m-%d"
        if c_step < 86400:
            dk = "%H"
            t_d = "%s h" % time.strftime("%s" % dk, time.localtime(float(stop))) 
        
        else:
            t_d = time.strftime("%s" % dk, time.localtime(float(stop))) 
        
        sel["tstamp"] = { "$gt": start , "$lte": stop }
        sel["rule_id"] =  {"$in": [sig] }
        #print sel
        res = coll_events.find(sel).distinct("peer_ip")
        
        i_val.append(len(res))
        i_desc.append(t_d)

    c_type = "area"
    chart_data["Uniq_IP_Count"] =  chart_dashboard_template % ("Uniq_IP_Count", c_type, "%s - %s Charts" % (sig, "Uniq-IPs"),"Uniq_IP",  json.dumps(i_desc), json.dumps(i_val) )


    # summary
    s_val = []
    s_desc = []
    for step in range((tnow - s_range), tnow, c_step):
        sel = {}
        #print "%s - %s - %s" % (step, tnow, c_step)
        start = step  
        stop  = step + c_step 
        #print "%s:%s - %s - %s" % (times, c_for, start, stop)
        
        # getting descriptor
        dk = "%m-%d"
        
        # getting descriptor
        dk = "%m-%d"
        if c_step < 86400:
            dk = "%H"
            t_d = "%s h" % time.strftime("%s" % dk, time.localtime(float(stop))) 
        
        else:
            t_d = time.strftime("%s" % dk, time.localtime(float(stop)))             

        sel["tstamp"] = { "$gt": start , "$lte": stop }
        sel["rule_id"] =  {"$in": [sig] }
        #print sel
        res = coll_events.find(sel).count()
        
        s_val.append(res)
        s_desc.append(t_d)

    c_type = "area"
    chart_data["Event_Count"] =  chart_dashboard_template % ("Event_Count", c_type, "%s - %s Charts" % (sig, "Summary"),"Event",  json.dumps(s_desc), json.dumps(s_val) )



    
    print sid_info

    return render_template(s_template, 
            page_header = phead, 
            sid  = sig, 
            hostlist = get_hostlist(),
            total_events = len(sig_list), 
            data=s_l, 
            uniq_ips = sorted(u_i, key=lambda u_i: u_i[1], reverse=True), #sorted(student_tuples, key=lambda student: student[2]) 
            uniq_events = sorted(u_e, key=lambda u_e: u_e[1], reverse=True), 
            display_all = d_all, 
            charts = chart_data, 
            sid_info = sid_info, 
            
            )


@app.route('/latest/',  methods=['GET'])
@app.route('/latest/<path:page>/',  methods=['GET'])
@requires_auth
@check_range
def d_latest(page = 0):

    s_range = si["range"]
    sel_dat = int(int(time.time() - s_range))

    
    if page == "ack":
        print "> ACK"
        latest = get_latest_sig()
        if len(latest) < 1:
            return(redirect("/latest-couldnt-check/"))    
        else:
            ul = latest["timestamp"]
            
#            update_user_session("latest_sig", ul)
            update_latest_user(ul)
            flash("sucessfully updated user_session / latest sig to :%s" % ul)
            app.jinja_env.globals['new_events'] = None
            return(redirect("/latest/new/"))
        return(redirect("/latest-not-known/--%s" % page))    
    
    elif page == "new":
        s_range = (si["latest_sig"] + 1)
        sel_dat =  s_range
        page = 0
    
    else:
        try:
            int(page)
        except:
            return(redirect("/latest/"))

    page_next, page_prev, skip = check_page(page)

    print "next: %s / prev: %s / skip_to: %s" % (page_next, page_prev, skip)



    total_events = len(nx_event.objects(tstamp__gte = sel_dat))

    p_total = int(total_events / d_limit)
    page_info = "%s / %s" % (page_next, p_total)

    sig_list    = nx_event.objects().skip(skip).order_by('-tstamp').limit(d_limit)
    uniq_ip = nx_event.objects(tstamp__gte = sel_dat).distinct("peer_ip")
    uniq_sig = nx_event.objects(tstamp__gte = sel_dat).distinct("rule_id")
    uniq_host = nx_event.objects(tstamp__gte = sel_dat).distinct("host")
    phead = "SID: Summary "  

    s_l = []
    u_i = []
    u_e = []
    u_h = []

    for s in sig_list:
        sid = s.rule_id
        date = s.dstamp
        url = url_convert(s.url)
        rip = s.peer_ip
        mz = s.zone
        var_name = s.var_name 
        host = s.host
        content = s.content
        idx = s.id
        tstamp = s.tstamp
        sensor = s.sensor_id

        try:
            s_msg = sigs_dict["%s" % sid][0]
        except:
            s_msg = "%s" % sid

        s_l.append((sid, s_msg, rip, url, date, mz, var_name, host, content, idx, tstamp, sensor))

    for i in uniq_ip:
        i_c = nx_event.objects(tstamp__gte = sel_dat, peer_ip = i).count()        
        u_i.append((i, i_c))

    for i in uniq_sig:
        i_c = nx_event.objects(tstamp__gte = sel_dat, rule_id = i).count()        
        u_e.append((i, i_c))

    for i in uniq_host:
        i_c = nx_event.objects(tstamp__gte = sel_dat, host = i).count()        
        u_h.append((i, i_c))



    return render_template("latest_display.html", 
            page_header = phead, 
            total_events = total_events,
            data=s_l, 
            uniq_ips = sorted(u_i, key=lambda u_i: u_i[1], reverse=True), #sorted(student_tuples, key=lambda student: student[2]) 
            uniq_events = sorted(u_e, key=lambda u_e: u_e[1], reverse=True), 
            uniq_hosts = sorted(u_h, key=lambda u_h: u_h[1], reverse=True), 
            page_next = page_next,
            page_prev = page_prev,
            page_info = page_info
            )

#~ @app.route('/d/',  methods=['GET'])
#~ @app.route('/d/<path:area>/',  methods=['GET'])
#~ @app.route('/d/<path:area>/<path:val>/',  methods=['GET'])
#~ @requires_auth
#~ @check_range
#~ def d_d_display(area=0, val=0, page=0):
    #~ print "d_display"
    #~ tnow = int(time.time())
    #~ if ip == 0:
        #~ return(redirect("/dashboard/"))
#~ 
    #~ try:
        #~ int(page)
    #~ except:
        #~ return(redirect("/dashboard/"))    
    #~ if page < 1:
        #~ skip = 0
        #~ page = 0
    #~ else:
        #~ skip = page * 100
#~ 
    #~ 
    #~ 
    #~ s_range = si["range"]
    #~ sel_dat = int(int(time.time() - s_range))
    #~ ip_list    = nx_event.objects(peer_ip = ip, tstamp__gte = sel_dat).skip(skip).order_by('-tstamp')
    #~ uniq_sid = nx_event.objects(peer_ip = ip, tstamp__gte = sel_dat).distinct("rule_id")
#~ 
    #~ i_l = []
    #~ u_e = []
#~ 
#~ 
    #~ for s in ip_list:
        #~ sid = s.rule_id
        #~ date = s.dstamp
        #~ url = s.url
        #~ rip = s.peer_ip
        #~ mz  = s.zone
        #~ var_name = s.var_name
        #~ host = s.host
        #~ content = s.content
        #~ idx = s.id
        #~ try:
            #~ s_msg = sigs_dict["%s" % sid][0]
        #~ except:
            #~ s_msg = "%s" % sid
#~ 
#~ 
        #~ try:
            #~ url = "%s" % url.encode("utf-8")
        #~ except:
            #~ url = "__unknown__"
#~ 
#~ 
        #~ i_l.append((sid, s_msg, rip, url, date, mz, var_name, host, content, idx))
#~ 
    #~ for i in uniq_sid:
        #~ i_c = nx_event.objects(rule_id = i, tstamp__gte = sel_dat, peer_ip = ip).count()
        #~ try:
            #~ i_msg = sigs_dict["%s" % i][0]
        #~ except:
            #~ i_msg = "%s" % i
#~ 
        #~ u_e.append((i, i_c, i_msg))
#~ 
    #~ # generating chart_data
    #~ chart_data = {}
    #~ 
    #~ if s_range < 86401:
        #~ c_step = 3600
    #~ else:
        #~ c_step = 86400
#~ 
    #~ # ip_count
    #~ i_val = []
    #~ i_desc = []
    #~ for step in range((tnow - s_range), tnow, c_step):
        #~ sel = {}
        #~ start = step  
        #~ stop  = step + c_step 
        #~ #print "%s:%s - %s - %s" % (times, c_for, start, stop)
        #~ 
        #~ # getting descriptor
        #~ dk = "%m-%d"
        #~ if c_step < 86400:
            #~ dk = "%H"
            #~ t_d = "%s h" % time.strftime("%s" % dk, time.localtime(float(stop))) 
        #~ 
        #~ else:
            #~ t_d = time.strftime("%s" % dk, time.localtime(float(stop))) 
        #~ 
        #~ sel["tstamp"] = { "$gt": start , "$lte": stop }
        #~ sel["peer_ip"] =  {"$in": [ip] }
        #~ res = coll_events.find(sel).count()
        #~ 
        #~ i_val.append(res)
        #~ i_desc.append(t_d)
#~ 
    #~ c_type = "area"
    #~ chart_data["IP_Event_Count"] =  chart_dashboard_template % ("IP_Event_Count", c_type, "%s - Events" % (ip),"Events",  json.dumps(i_desc), json.dumps(i_val) )
#~ 
#~ 
    #~ # uniq_sids
    #~ s_val = []
    #~ s_desc = []
    #~ for step in range((tnow - s_range), tnow, c_step):
        #~ sel = {}
#~ #        print "%s - %s - %s" % (step, tnow, c_step)
        #~ start = step  
        #~ stop  = step + c_step 
        #~ #print "%s:%s - %s - %s" % (times, c_for, start, stop)
        #~ 
        #~ # getting descriptor
        #~ dk = "%m-%d"
        #~ 
        #~ # getting descriptor
        #~ dk = "%m-%d"
        #~ if c_step < 86400:
            #~ dk = "%H"
            #~ t_d = "%s h" % time.strftime("%s" % dk, time.localtime(float(stop))) 
        #~ 
        #~ else:
            #~ t_d = time.strftime("%s" % dk, time.localtime(float(stop)))             
#~ 
        #~ sel["tstamp"] = { "$gt": start , "$lte": stop }
        #~ sel["peer_ip"] =  {"$in": [ip] }
        #~ res = coll_events.find(sel).distinct("rule_id")
        #~ 
        #~ s_val.append(len(res))
        #~ s_desc.append(t_d)
#~ 
    #~ c_type = "area"
    #~ chart_data["Uniq_Events"] =  chart_dashboard_template % ("Uniq_Events", c_type, "%s - Uniq Events" % (ip),"Uniq_Events",  json.dumps(s_desc), json.dumps(s_val) )
    #~ phead = "IP: %s :: [ %s ]" % (ip, len(ip_list)) 
#~ 
    #~ try:
        #~ ip_name = socket.gethostbyaddr(ip)
    #~ except:
        #~ ip_name = ip
    #~ 
    #~ ip_info = [ip, ip_name]
    #~ 
    #~ return render_template('ip_display.html', 
            #~ page_header = phead, 
            #~ total_events = len(ip_list), 
            #~ data=i_l, 
            #~ uniq_events = sorted(u_e, key=lambda u_e: u_e[1], reverse=True),         
            #~ ip_ip = ip,
            #~ charts = chart_data,
            #~ ip_info = ip_info, 
             #~ 
            #~ )
    #~ 
#~ 
    #~ 

# default error handlers

@app.route('/ip/',  methods=['GET'])
@app.route('/ip/<path:ip>/',  methods=['GET'])
@app.route('/ip/<path:ip>/<path:page>/',  methods=['GET'])
@requires_auth
@check_range
def d_ip_display(ip=0, page=0):
    return(redirect("/do/search/ip@%s/" % ip))

    print "ip_display"
    tnow = int(time.time())
    if ip == 0:
        return(redirect("/dashboard/"))

    try:
        int(page)
    except:
        return(redirect("/dashboard/"))    
    if page < 1:
        skip = 0
        page = 0
    else:
        skip = page * 100

    
    
    s_range = si["range"]
    sel_dat = int(int(time.time() - s_range))
    ip_list    = nx_event.objects(peer_ip = ip, tstamp__gte = sel_dat).skip(skip).order_by('-tstamp')
    uniq_sid = nx_event.objects(peer_ip = ip, tstamp__gte = sel_dat).distinct("rule_id")

    i_l = []
    u_e = []


    for s in ip_list:
        sid = s.rule_id
        date = s.dstamp
        url = s.url
        rip = s.peer_ip
        mz  = s.zone
        var_name = s.var_name
        host = s.host
        content = s.content
        idx = s.id
        try:
            s_msg = sigs_dict["%s" % sid][0]
        except:
            s_msg = "%s" % sid


        try:
            url = "%s" % url.encode("utf-8")
        except:
            url = "__unknown__"


        i_l.append((sid, s_msg, rip, url, date, mz, var_name, host, content, idx))

    for i in uniq_sid:
        i_c = nx_event.objects(rule_id = i, tstamp__gte = sel_dat, peer_ip = ip).count()
        try:
            i_msg = sigs_dict["%s" % i][0]
        except:
            i_msg = "%s" % i

        u_e.append((i, i_c, i_msg))

    # generating chart_data
    chart_data = {}
    
    if s_range < 86401:
        c_step = 3600
    else:
        c_step = 86400

    # ip_count
    i_val = []
    i_desc = []
    for step in range((tnow - s_range), tnow, c_step):
        sel = {}
        start = step  
        stop  = step + c_step 
        #print "%s:%s - %s - %s" % (times, c_for, start, stop)
        
        # getting descriptor
        dk = "%m-%d"
        if c_step < 86400:
            dk = "%H"
            t_d = "%s h" % time.strftime("%s" % dk, time.localtime(float(stop))) 
        
        else:
            t_d = time.strftime("%s" % dk, time.localtime(float(stop))) 
        
        sel["tstamp"] = { "$gt": start , "$lte": stop }
        sel["peer_ip"] =  {"$in": [ip] }
        res = coll_events.find(sel).count()
        
        i_val.append(res)
        i_desc.append(t_d)

    c_type = "area"
    chart_data["IP_Event_Count"] =  chart_dashboard_template % ("IP_Event_Count", c_type, "%s - Events" % (ip),"Events",  json.dumps(i_desc), json.dumps(i_val) )


    # uniq_sids
    s_val = []
    s_desc = []
    for step in range((tnow - s_range), tnow, c_step):
        sel = {}
#        print "%s - %s - %s" % (step, tnow, c_step)
        start = step  
        stop  = step + c_step 
        #print "%s:%s - %s - %s" % (times, c_for, start, stop)
        
        # getting descriptor
        dk = "%m-%d"
        
        # getting descriptor
        dk = "%m-%d"
        if c_step < 86400:
            dk = "%H"
            t_d = "%s h" % time.strftime("%s" % dk, time.localtime(float(stop))) 
        
        else:
            t_d = time.strftime("%s" % dk, time.localtime(float(stop)))             

        sel["tstamp"] = { "$gt": start , "$lte": stop }
        sel["peer_ip"] =  {"$in": [ip] }
        res = coll_events.find(sel).distinct("rule_id")
        
        s_val.append(len(res))
        s_desc.append(t_d)

    c_type = "area"
    chart_data["Uniq_Events"] =  chart_dashboard_template % ("Uniq_Events", c_type, "%s - Uniq Events" % (ip),"Uniq_Events",  json.dumps(s_desc), json.dumps(s_val) )
    phead = "IP: %s :: [ %s ]" % (ip, len(ip_list)) 

    try:
        ip_name = socket.gethostbyaddr(ip)
    except:
        ip_name = ip
    
    ip_info = [ip, ip_name]
    
    return render_template('ip_display.html', 
            page_header = phead, 
            total_events = len(ip_list), 
            data=i_l, 
            uniq_events = sorted(u_e, key=lambda u_e: u_e[1], reverse=True),         
            ip_ip = ip,
            charts = chart_data,
            ip_info = ip_info, 
             
            )
    

    

# default error handlers

@app.errorhandler(404)
def page_not_found(error):
    return render_template('error.html', code=404, error=error), 404

@app.errorhandler(403)
def access_denied(error):
    return render_template('error.html', code=403, error=error), 403

@app.errorhandler(500)
def error500(error):
    return render_template('error.html',  code=500, error=error), 500
    

@app.errorhandler(501)
def error501(error):
    return render_template('error.html',  code=501, error=error), 501
    

@app.errorhandler(502)
def error502(error):
    return render_template('error.html', code=502, error=error), 502
    
@app.errorhandler(503)
def error503(error):
    return render_template('error.html', code=503, error=error), 503

def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    
    return(check_login(username, password))

def check_login(user, pw):
    try:
        print au[user]['name']
    except:
        return(False)
    if user in au:
        cpw = PwCheck(user, pw)
        print cpw.pw_hash
        if cpw.pw_hash == au[user]['pass']:
            print "[+] > user_login: %s" % au[user]['name']
            return(True)
        else:
            print "[-] > wrong pw for user: %s" % au[user]['name']
            return(False)

    else:
        print "[-] > user not found: %s" % user
        return(False)


    try:
        if user in au:
            if pw == au[user]["pass"]:                
                print "[+] > user_login: %s" % au[user]
                return(True)
            else:
                print "[-] > wrong pw for user: %s" % au[user]
                return(False)

        else:
            print "[-] > user nor found: %s" % user
            return(False)
    except:
        print "[-] > ERROR while checking user: %s" % user
        return(False)
        
        
    
    return(False)


def update_user_session(field, value):

    global si

    print "> updating user_session: %s | %s" % (field, value)

    so = si
    if field not in so:
        print "> cannot update user_session: %s | %s" % (field, value)
        return(1)

    so[field] = value

    mc.set("%s.session" % sess, so)
    print "> OK updating user_session: %s | %s" % (field, value)
    
    si = so

def undo_reload_header():
    app.jinja_env.globals['reload_header'] = ""

def update_latest_user(timestamp):

        print "> updating user_session_info"
        update_user_session("latest_sig", timestamp)
        user_stat_file = "%s/users/%s.data/latest.sig" % (d_data, si["name"])
        f = open(user_stat_file, "w")
        f.write("""latest sig   : %s
written     : %s""" % (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(float(timestamp))), time.strftime("%Y-%m-%d %H:%M:%S") ))
        f.close()
        tnow = int(time.time())
        print "> updating user_session_file"
        os.utime(user_stat_file,(tnow, int(timestamp)))
        



def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
    'Please Login', 401,
    {'WWW-Authenticate': 'Basic realm="Login"'})
    

if __name__ == '__main__':
    app.config["DEBUG"] = DEBUG 
    mc = memcache.Client(['127.0.0.1:11211'], debug=0)
    
    mc_key = app.config['SECRET']
    d_limit = app.config["DISPLAY_LIMIT"]
    d_host  = app.config["DEFAULT_HOST"]
    d_data  = app.config['DATA_DIR']
    cache_time = app.config['MEMCACHE_CACHETIME']
    d_robot = app.config['ROBOT'] 
    init()

    au = json.loads(mc.get("%s.users" % mc_key))

    msg_admin("[+] system startup at %s " % time.strftime("%Y-%m-%d %H:%M:%S"))

    page_next = None 
    page_prev = None

    from mongoengine import connect, Q

    from naxsi_db_schema import Event as nx_event
    from naxsi_db_schema import Temp as tmp
    from naxsi_db_schema import Agent as agents
    from naxsi_db_schema import AgentLog as alog 
    from naxsi_db_schema import UserData as udata
    from naxsi_db_schema import Messages as msg
    
    if not os.path.isdir(doxi_rules_dir):
        if os.path.isdir("../%s" % doxi_rules_dir):
            doxi_rules_dir = "../%s" % doxi_rules_dir

    from pymongo import MongoClient, DESCENDING as DESC
    client = MongoClient(host = app.config['MONGODB_HOST'], port = app.config['MONGODB_PORT'])
    db = client.naxsi_events
    coll_events = db.events
    coll_tmp = db.doxi_temp
    coll_udata = db.doxi_user_data
    
    
    # generating and ensuring indizes
    print "> generating indizes"
    db.events.ensure_index( 'rule_id' )
    db.events.ensure_index( 'peer_ip' )
    db.events.ensure_index( 'url' )
    db.events.ensure_index( 'host' )
    db.events.ensure_index( 'var_name' )
    db.events.ensure_index( 'mz' )
    db.events.ensure_index( 'tstamp' )

    db.events.ensure_index( 'rule_id' , 'tstamp' )
    db.events.ensure_index( 'peer_ip' , 'tstamp' )
    db.events.ensure_index( 'url' , 'tstamp' )
    db.events.ensure_index( 'host' , 'tstamp' )
    db.events.ensure_index( 'var_name' , 'tstamp' )
    db.events.ensure_index( 'mz' , 'tstamp' )
    print "  > index.done"
    

    
    sigs_dict = sigs_parse(glob.glob("%s/*.rules" % doxi_rules_dir))
    app.jinja_env.globals['sigs_dict'] = sigs_dict

    
    app.run(host="0.0.0.0")



