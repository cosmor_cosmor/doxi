
<div align="left" markdown="1">

# DX-Console - Documentation

## Features

- [See here](/doc/features/)


## Setup

- [See here](/doc/setup/)

<a name="#agents"></a>
## Agents

Agents are very basic atm, just some kind of try&error to find a nice path
to define running agents that alert when certain thresholds are met (event_count per
1hr / 24hrs etc)

- [See more here](/doc/agents)


## User_Administration

- [See more here](/doc/users/)

## Api

- [See more here](/doc/api/)


### Mongo-DB-Schema

- db: naxsi_events
- collections:
    - events -> mirror of naxsi_sig, container for all generated events
        - peer_ip
        - rule_id
        - url
        - mz
        - arg
        - content
        - host
        - tstamp ( int(time()) )
        - dstamp (date as string)
        
    - doxi_tmp -> temporary stats / dashboard etc, might be deleted w/out impacts
    - doxi_stats -> status-data for stuff like known hosts/urls etc, can be deleted, but will be filled on next dx-robot/console-run
    - doxi_messages -> system-messages for admins and messages for new found urls/hosts for all
    - doxi_user_data -> user-store like fp-list, saved searches etc
    

### Search - Hints


- [See more here](/doc/search_hints/)
    


