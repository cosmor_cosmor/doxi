#!/bin/bash
#
# 
# v2013-07-02
#

echo "

> DOXI_TOOLS/RULES - Installer 

"

if [ -d doxi-rules ]; then
    echo "> deleting exiting doxi-rules-dir; 
> you'll find a copy in doxi-rules.orig.tgz
    "
    tar czf doxi-rules.orig.tgz doxi-rules
    rm -Rf doxi-rules
fi




if [ ! -f doxi.conf ]; then

    cp doxi.conf.template doxi.conf 
    vi doxi.conf


fi

if [ ! -d doxi-rules ]; then

    mkdir doxi-rules


fi

if [ ! -d /etc/nginx/doxi-rules ]; then

    mkdir /etc/nginx/doxi-rules


fi

echo "#
# local.rules for naxi - shall never be touched by rules-updates 
#

" > doxi-rules/local.rules

echo "#
# creating a sample rules.conf 
#

"



cd $HOME/doxi && ./dx-update -s

echo "#
# creating a sample rules.conf 
#

"

cd $HOME/doxi && cp doxi-rules/rules.conf.default doxi-rules/rules.conf && vi doxi-rules/rules.conf 


cd $HOME/doxi && ./dx-update -s


echo "

> doxi-installation done, please adjust as needed 
> HINT: 
  - nginx.conf
  - rules.conf
  - local.rules
  - server {} - configs

"





