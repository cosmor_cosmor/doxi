

chart_dashboard_template = """


    chart: {
        renderTo: '%s',
        type: '%s'
    },
    title: {
        text: '%s'
    },

    yAxis: {
        min: 0,
        title: {
            text: '%s',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        crosshairs: true,
        shared: true

    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },

    credits: {
        enabled: false
    },
    xAxis: {
        categories: %s,
        title: {
            text: null
        }
    },

        series: [{
            name: 'Count',
            data: %s
        }]




"""
